import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule, MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {CreateInvoiceComponent} from './components/create/CreateInvoiceComponent';
import {DataService} from './services/data.service';
import {UpdateInvoiceComponent} from './components/update/UpdateInvoiceComponent';
import {DeleteInvoiceComponent} from './components/delete/DeleteInvoiceComponent';

// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    CreateInvoiceComponent,
    UpdateInvoiceComponent,
    DeleteInvoiceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  entryComponents: [
    CreateInvoiceComponent,
    UpdateInvoiceComponent,
    DeleteInvoiceComponent,

  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

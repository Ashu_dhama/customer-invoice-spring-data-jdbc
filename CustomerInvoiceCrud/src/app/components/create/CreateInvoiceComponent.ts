import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {FormControl, Validators} from '@angular/forms';
import {Invoice} from '../../models/Invoice';



@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app.create',
  templateUrl: './CreateInvoiceComponent.html',
  styleUrls: ['./CreateInvoiceComponent.css']
})

export class  CreateInvoiceComponent {
  constructor(public dialogRef: MatDialogRef<CreateInvoiceComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Invoice,
              public dataService: DataService) {}

  formControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[0-9]{1,5}'),
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required' || 'pattern') ? 'Please enter valid input' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
   this.dataService.createInvoice(this.data);
  }
}

import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataService} from '../../services/data.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app.delete',
  templateUrl: './DeleteInvoiceComponent.html',
  styleUrls: ['./DeleteInvoiceComponent.css']
})

export class DeleteInvoiceComponent {
  constructor(public dialogRef: MatDialogRef<DeleteInvoiceComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.dataService.deleteInvoice(this.data.invoiceNumber);
  }
}


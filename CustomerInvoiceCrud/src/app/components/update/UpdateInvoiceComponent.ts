import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataService} from '../../services/data.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app.update',
  templateUrl: './UpdateInvoiceComponent.html',
  styleUrls: ['./UpdateInvoiceComponent.css']
})

export class UpdateInvoiceComponent {
  constructor(public dialogRef: MatDialogRef<UpdateInvoiceComponent>,
@Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService) {}

formControl = new FormControl('', [
  Validators.required,
  Validators.pattern('[0-9]{1,5}'),
]);

getErrorMessage() {
  return this.formControl.hasError('required' || 'pattern') ? 'Please enter valid input' :
    this.formControl.hasError('email') ? 'Not a valid email' :
      '';
}

submit() {
  // emppty stuff
}

onNoClick(): void {
  this.dialogRef.close();
}

stopEdit(): void {
  this.dataService.updateInvoice(this.data);
}
}

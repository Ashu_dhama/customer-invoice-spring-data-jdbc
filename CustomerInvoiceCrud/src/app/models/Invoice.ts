export class Invoice {
  id: number;
  invoiceNumber: string;
  customerName: string;
  product: number;
  totalAmount: number;
  dueAmount: number;
  issuedOn: string;
  deadline: string;
  status: number;
}

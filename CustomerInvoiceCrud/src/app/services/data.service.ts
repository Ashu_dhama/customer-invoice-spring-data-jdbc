import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Invoice} from '../models/Invoice';

@Injectable()
export class DataService {
  private readonly API_URL = 'http://localhost:8080/api/invoices/';

  dataChange: BehaviorSubject<Invoice[]> = new BehaviorSubject<Invoice[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient) {}

  get data(): Invoice[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllInvoices(): void {
    this.httpClient.get<Invoice[]>(this.API_URL).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        console.log (error.name + ' ' + error.message);
      });
  }
  createInvoice(invoice: Invoice): void {
    // tslint:disable-next-line:one-line
    this.httpClient.post(this.API_URL, invoice).subscribe(data => {
      console.log(data);
    });
    this.dialogData = invoice;
  }

  updateInvoice(invoice: Invoice): void {
    this.httpClient.put(this.API_URL + 'update/', invoice).subscribe(
      data => {
        console.log(data);
      }
    );
    this.dialogData = invoice;
  }

  deleteInvoice(invoiceNumber: string): void {
    this.httpClient.delete(this.API_URL + invoiceNumber).subscribe(data => {
      console.log(data);
    });
    console.log(invoiceNumber);
  }
}





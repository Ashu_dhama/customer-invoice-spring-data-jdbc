package com.customerInvoice.Customerinvoiceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerInvoiceAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerInvoiceAppApplication.class, args);
	}

}

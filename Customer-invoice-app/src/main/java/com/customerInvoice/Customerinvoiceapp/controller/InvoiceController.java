package com.customerInvoice.Customerinvoiceapp.controller;

import com.customerInvoice.Customerinvoiceapp.Dao.InvoiceDaoImpl;
import com.customerInvoice.Customerinvoiceapp.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class InvoiceController {
    @Autowired
    private InvoiceDaoImpl invoiceDaoImpl;

    @PostMapping("/invoices")
    @Transactional
    public Invoice createInvoice(@RequestBody Invoice invoice) {
        try {
            invoiceDaoImpl.save(invoice);
            return invoice;
        } catch (Exception e) {
            e.printStackTrace();
            return invoice;
        }
    }

    @GetMapping("/invoices")
    @Transactional
    public List<Invoice> getAllInvoices() {
        try {
            return (List<Invoice>) invoiceDaoImpl.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<Invoice>();
        }

    }

    @PutMapping("/invoices/update")
    @Transactional
    public void updateInvoice(@RequestBody Invoice invoice) {
        try {
            invoiceDaoImpl.update(invoice);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DeleteMapping("/invoices/{invoiceNumber}")
    public String deleteInvoice(@PathVariable("invoiceNumber") String invoiceNumber) {
        try {
            invoiceDaoImpl.deleteByInvoiceNumber(invoiceNumber);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "fail";
        }
    }
}

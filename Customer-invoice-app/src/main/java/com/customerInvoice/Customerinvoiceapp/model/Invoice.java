package com.customerInvoice.Customerinvoiceapp.model;

import java.util.Date;

import lombok.*;
import org.springframework.data.annotation.Id;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    @Id
    private String invoiceNumber;
    private String customerName;
    private String product;
    private double totalAmount;
    private double dueAmount;
    private Date issuedOn;
    private Date deadline;
    private String status;

    @Override
    public String toString() {
        return "invoice [invoiceNumber=" + invoiceNumber + ", customerName=" + customerName + ", product=" + product + ", totalAmount=" + totalAmount + ", dueAmount=" + dueAmount + ", deadline=" + deadline + ", status=" + status + ", issuedOn=" + issuedOn + ",]";
    }
}

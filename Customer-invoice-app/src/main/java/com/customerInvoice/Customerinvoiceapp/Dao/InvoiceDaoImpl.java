package com.customerInvoice.Customerinvoiceapp.Dao;

import com.customerInvoice.Customerinvoiceapp.model.Invoice;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

@Service
public class InvoiceDaoImpl{
    private JdbcTemplate jdbcTemplate;

    public InvoiceDaoImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public int save(Invoice invoice) {
        String sql = "INSERT INTO invoice (customer_Name, product, total_Amount, due_Amount, issued_On, deadline, status) VALUES (?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, invoice.getCustomerName(), invoice.getProduct(), invoice.getTotalAmount(), invoice.getDueAmount(), invoice.getIssuedOn(), invoice.getDeadline(), invoice.getStatus());
    }

    public List<Invoice> findAll() {
        String sql = "SELECT * FROM Invoice";
        RowMapper<Invoice> rowMapper = (resultSet, i) -> {
            String invoiceNumber = resultSet.getString("invoice_Number");
            String customerName = resultSet.getString("customer_Name");
            String product = resultSet.getString("product");
            double totalAmount = resultSet.getDouble("total_Amount");
            double dueAmount = resultSet.getDouble("due_Amount");
            Date issuedOn = resultSet.getDate("issued_On");
            Date deadline = resultSet.getDate("deadline");
            String status = resultSet.getString("status");

            return new Invoice(invoiceNumber, customerName, product, totalAmount, dueAmount, issuedOn, deadline, status);
        };
        return jdbcTemplate.query(sql, rowMapper);
    }

    public int update(Invoice invoice) {
        String sql = "UPDATE Invoice SET customer_Name= ?, product=?, total_Amount=?, due_Amount=?, issued_On=?, deadline=?, status=? ";
        return jdbcTemplate.update(sql, invoice.getCustomerName(), invoice.getProduct(), invoice.getTotalAmount(), invoice.getDueAmount(), invoice.getIssuedOn(), invoice.getDeadline(), invoice.getStatus());
    }

    public int deleteByInvoiceNumber(String invoiceNumber) {
        String sql = "DELETE FROM Invoice WHERE invoice_Number=" + invoiceNumber;
        return jdbcTemplate.update(sql);
    }
}


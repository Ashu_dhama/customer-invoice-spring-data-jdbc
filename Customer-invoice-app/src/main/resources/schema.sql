DROP table if exists invoice;

create table invoice(
invoice_Number varchar (255) NOT NULL AUTO_INCREMENT ,
customer_Name varchar (50),
product varchar (50),
total_Amount int (60),
due_Amount int (60),
issued_On date,
deadline date,
status varchar (50),
PRIMARY KEY (invoice_Number)
);

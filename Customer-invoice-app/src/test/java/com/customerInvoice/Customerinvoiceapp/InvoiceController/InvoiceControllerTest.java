package com.customerInvoice.Customerinvoiceapp.InvoiceController;

import com.customerInvoice.Customerinvoiceapp.model.Invoice;
import com.customerInvoice.Customerinvoiceapp.Dao.InvoiceDaoImpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest
public class InvoiceControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InvoiceDaoImpl invoiceDaoImpl;

    @Test
    public void createInvoiceTest() throws Exception {
        Invoice invoice = new Invoice();
        invoice.setCustomerName("customer");
        invoice.setProduct("product");
        invoice.setTotalAmount(22);
        invoice.setDueAmount(11);
        invoice.setStatus("due");
        String inputInJson = this.mapToJson(invoice);
        String Url = "/api/invoices";

        Mockito.when(invoiceDaoImpl.save(Mockito.any(Invoice.class))).thenReturn(1);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(Url)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();
        assertThat(outputInJson).isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());

    }

    @Test
    public void testGetAllInvoice() throws Exception {
        Invoice invoice = new Invoice();
        invoice.setCustomerName("customer");
        invoice.setProduct("product");
        invoice.setTotalAmount(22);
        invoice.setDueAmount(11);
        invoice.setStatus("due");
        String inputInJson = this.mapToJson(invoice);
        List<Invoice> invoiceList = new ArrayList<>();
        invoiceList.add(invoice);

        Mockito.when(invoiceDaoImpl.findAll()).thenReturn(invoiceList);
        String Url = "/api/invoices";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(Url)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(invoiceList);

        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);

    }

    @Test
    public void deleteInvoiceTest() throws Exception {
        String Url = "/api/invoices/3";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete(Url)).andReturn();

        int status = mvcResult.getResponse().getStatus();

        assertEquals(HttpStatus.OK.value(), status);
    }

    @Test
    public void testUpdateInvoice() throws Exception {
        String Url = "/api/invoices/update";
        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber("2");
        invoice.setStatus("due");
        invoice.setDueAmount(11);
        invoice.setTotalAmount(22);
        invoice.setProduct("product");
        invoice.setCustomerName("customer");

        Mockito.when(invoiceDaoImpl.update(Mockito.any(Invoice.class))).thenReturn(1);
        invoice.setStatus("paid");
        String inputJson = this.mapToJson(invoice);
        System.out.println(inputJson);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(Url)
                .accept(MediaType.APPLICATION_JSON).content(inputJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String outputInJson = response.getContentAsString();
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }


    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}
